#Fair Ratios Approach 1:
------------

```python
def even(k: int) -> bool:
    return k % 2 == 0

def verify(queue: list[int]) -> str:
    if not even(sum(queue)):
        return "NO"
    else:
        return str(distribute(queue))
    
def distribute(queue: list[int]) -> int:
    if len(queue) == 1:
        return 0 
    elif even(queue[0]):
        return distribute(queue[1:])
    else:
        queue = [queue[1] + 1] + queue[2:]
        return 2 + distribute(queue)

print(verify([1,2,3,4]))

#Approach 2:
------------

def encode(queue: list[int]) -> list:
    oe = {True: "E", False: "O"}
    return ''.join([oe[i % 2 == 0] for i in queue])
    
def check_and_distribution(queue: list[int]) -> str:
    if sum(queue) % 2 == 1:
        return "NO"
    else:
        return str(distribute(encode(queue)))
        
def distribute(queue: str) -> int:
    spans = queue.split('O')[1::2]
    return sum([2 + 2 * len(span) for span in spans])

print(check_and_distribution([2, 3, 4, 5, 6]))
print(check_and_distribution([2, 3, 4, 5, 7]))

#polynomial problem - Approach
-----------------------------

```python
PLUS, MINUS = "+", "-"
CARET, SPACE = "^", " "

def mathify(s: str) -> str:
    return ' '.join([make_term(t, get_var(s)) for t in new_terms(s)])

def get_var(poly: str) -> str:
    for ch in poly:
        if ch.isalpha():
            return ch

def terms(poly: str) -> list[str]:
    poly = ''.join([ch for ch in poly if ch != SPACE])
    if poly[0].isdigit():
        poly = PLUS + poly
    poly = poly.replace(MINUS, PLUS + MINUS)
    return poly.split(PLUS)

def reformat_term(term: str, var: str) -> tuple[int, int]:
    if var not in term:
        return 0, int(term)  
    else:
        coefficient, rest = term.split(var)
        if CARET not in rest:
            return 1, int(coefficient)
        else:
            return int(rest[1:]), int(coefficient)

def new_terms(poly: str) -> list[tuple[int, int]]:
    return sorted([reformat_term(t, get_var(poly)) for t in terms(poly)[1:]])

def make_term(nt: tuple[int, int], var: str) -> str:
    sign = PLUS if nt[1] > 0 else MINUS
    if nt[0] == 0:
        return str(nt[1])
    elif nt[0] == 1:
        if nt[1] == 1:
            return sign +" "+ var
        else:
            return sign +" "+ str(abs(nt[1])) + var
    else:
        if nt[1] == 1:
            return sign +" " + var + CARET + str(nt[0])
        else:
            return sign +" "+ str(abs(nt[1])) + var + CARET + str(nt[0])

print(mathify("-7x^2 + 3x - 5"))

#pattern - Approach
------------------

STAR = "#"
SPACE = "_"
LF = "\n"
START, REPEAT , END = STAR, SPACE + SPACE, "\b" + STAR
def pattern(n: int):
    return (line(i)  for i in range(n))
def line(line_num: int):
    return start()+ repeat(line_num)+ end()
def start():
    return START
def repeat(line_num: int):
    return REPEAT * line_num
def end():
    return END
def pyramid(n: int):
    s = LF.join(line.center(40) for line in pattern(n))
    print(s)
    
pyramid(5)


