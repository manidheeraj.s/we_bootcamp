## Day 1
=======

**1. Problem Statement**: Fair Ratios

As the ruler of Rankhacker Castle, you're tasked with distributing bread to your subjects. Each person must have an even number of loaves, and when you give bread to someone, you must also give it to the person immediately in front or behind them. Given the initial distribution of loaves, determine the minimum loaves needed to satisfy these rules. If impossible, output "NO".

**Input**:

Array of integers (0-1000) representing loaves held by each citizen.

**Output**:

Minimum loaves needed or "NO" if impossible.

## Day 2
====== 

**3. Problem Statement:**

Develop a Python program to classify strings based on their character order patterns. 
The program should include the following functionalities:
Classify strings based on their order patterns, returning 'A' for ascending, 'D' for descending,
 'P' for ascending-then-descending, 'V' for descending-then-ascending, and 'X' for unclassified.

**Problem**: Patterns

```plaintext
---------
    * 
   * * 
  * * * 
 * * * * 
* * * * * 
 * * * * 
  * * * 
   * * 
    * 

*
**
***
****
*****
```
---------
## Day 3

**Problem**: Crosswords

## Day 4

**Problem Statement** : Chess pieces

1. There are four pieces:
   - **Bishop**: Moves diagonally.
   - **Knight**: Moves in an L shape.
   - **Rook**: Moves straight (horizontally or vertically).
   - **Queen**: Can move like a bishop, rook, or knight.

2. Rows are called ranks and columns are called files.

3. The input is a tuple of rank and files which is the position of the current piece and the rank and files of the other piece.

4. Task:
   - a. Given a piece and a position on the empty board, calculate all the possible attack positions of the piece.
   - b. Given a piece `x` and a position `p1` and another piece `y` and position `p2`, calculate whether `x` attacks `y` and vice-versa.
